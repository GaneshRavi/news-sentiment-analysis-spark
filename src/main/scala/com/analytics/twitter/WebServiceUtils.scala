package com.analytics.twitter

import java.net.{URL, HttpURLConnection}

/**
 * @author gravi
 */
object WebServiceUtils {

	def get(url: String,
			connectTimeout:Int = 5000,
			readTimeout:Int = 5000,
			requestMethod: String = "GET") = {
		try {
			val connection = (new URL(url)).openConnection.asInstanceOf[HttpURLConnection]
					connection.setConnectTimeout(connectTimeout)
					connection.setReadTimeout(readTimeout)
					connection.setRequestMethod(requestMethod)
					val inputStream = connection.getInputStream
					val content = scala.io.Source.fromInputStream(inputStream).mkString
					if (inputStream != null) inputStream.close
					content
		} catch {
  		case ioe: java.io.IOException => println(ioe.printStackTrace());
  		case ste: java.net.SocketTimeoutException => (ste.printStackTrace());
		}
	}

}