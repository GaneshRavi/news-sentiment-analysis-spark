package com.analytics.stream.receivers

import org.apache.spark.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver
import twitter4j.Status
import twitter4j.StatusDeletionNotice
import twitter4j.StatusListener
import twitter4j.TwitterStream
import twitter4j.TwitterStreamFactory
import twitter4j.auth.AccessToken
import twitter4j.StallWarning
import twitter4j.conf.ConfigurationBuilder
import twitter4j.conf.Configuration
import twitter4j.FilterQuery
import twitter4j.GeoLocation

class TweetStreamReceiver(conf: Configuration, filters: Array[String])
extends Receiver[Status](StorageLevel.MEMORY_AND_DISK_2) with Logging {

	def this(consumerKey: String, consumerSecret: String, accessToken: String, accessTokenSecret: String, filters: Array[String]) = this(
			new ConfigurationBuilder().setOAuthConsumerKey(consumerKey)
			.setOAuthConsumerSecret(consumerSecret)
			.setOAuthAccessToken(accessToken)
			.setOAuthAccessTokenSecret(accessTokenSecret).build(), filters)

			@volatile
			var currentTwitterStream: TwitterStream = _;

	def onStart() {
		receive();
	}

	def onStop() {
		setTwitterStream(null)
		logInfo("Tweet receiver stopped")
	}

	private def setTwitterStream(newTwitterStream: TwitterStream) = synchronized {
		if (currentTwitterStream != null) {
			currentTwitterStream.shutdown()
		}
		currentTwitterStream = newTwitterStream
	}

	private def receive() {
		try {
			val tempTwitterStream = new TwitterStreamFactory(conf).getInstance();

			tempTwitterStream.addListener(new StatusListener() {
				def onStatus(status: Status) {
					if(status.getLang().equals("en")) {
						//println(status.getText());
						store(status);
					}
				}
				def onDeletionNotice(statusDeletionNotice: StatusDeletionNotice) {}
				def onTrackLimitationNotice(numberOfLimitedStatuses: Int) {}
				def onScrubGeo(userId: Long, upToStatusId: Long) {}
				def onStallWarning(stallWarming: StallWarning) {}
				def onException(ex: Exception) {
					if (!isStopped()) {
						restart("Stopped receiving tweets. Restarting receiver", ex)
					}
				}
			});

			val filterQuery = new FilterQuery
					// Source: https://wiki.ceh.ac.uk/display/cehigh/Bounding+box
					val long1 = -9.23;
			val long2 = 2.69;
			val lat1 = 49.84;
			val lat2 = 60.85;

			val locations = Array(Array(long1, lat1), Array(long2, lat2));
			filterQuery.locations(locations: _*);
			tempTwitterStream.filter(filterQuery);

			setTwitterStream(tempTwitterStream)
			logInfo("Tweet receiver successfully started")
		} catch {
		  case e: Exception => restart("Error starting tweet receiver", e)
		}

	}
}