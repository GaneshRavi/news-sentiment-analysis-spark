Building a fat jar
------------------
To build a fat jar, execute the following command from the root of the project directory:

	$ mvn package
	
To submit as a Spark job
------------------------
    $ /opt/spark-1.4.1/bin/./spark-submit --class com.analytics.twitter.SentimentClassifier \
        --master local[*] \
        --driver-memory 4g \
        --executor-memory 2g \
        target/sentiment-analysis-1.0.0-SNAPSHOT-jar-with-dependencies.jar \
        <CONSUMER_KEY> <CONSUMER_SECRET> \
        <ACCESS_TOKEN> <ACCESS_TOKEN_SECRET> \
        <TWEET_FILTERS>

Please use the consumer key, consumer secret, access token and access token secret of your Twitter application. This can be obtained by creating a new application at https://apps.twitter.com/app/new.  
Optionally filters can be used for subscribing to Tweets. Filters are the words that we are interested in, so only the Tweets containing at least one of those words will be sent to our subscribed stream. Each filter word must be separated by a space.